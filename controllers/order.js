const User = require("../models/User");
const Order = require("../models/Order");

module.exports.orderAProducts = async (req, res) => {
  console.log(req.user.id);

  if (req.user.isAdmin) {
    return res.status(403).send("Action Forbidden"); // Use 403 for forbidden actions
  }

  try {
    const user = await User.findById(req.user.id);

    let order = await Order.findOne({
        "Order.email": req.user.email
    });
    // console.log(req.user.email)

    // Create a new order if it doesn't exist
  if (!order) {
    order = new Order({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      mobileNo: user.mobileNo,
      orderedProducts: [
        {
          products: [],
          totalAmount: 0, // Initialize totalAmount to 0
        },
      ],
    });
  }

    let totalOrderAmount = 0;
    const requestBody = req.body;

    if (Array.isArray(requestBody) && requestBody.length >= 1) {
      // Loop through the products in the request body
      for (let i = 0; i < requestBody.length; i++) {
        const product = requestBody[i];

        const orderTotalAmount = product.price * product.quantity
        console.log(orderTotalAmount)

        // Add the product to the user's order based on its index
        order.orderedProducts[0].products.push({
          productId: product.product_id,
          name: product.name,
          price: product.price,
          quantity: product.quantity,
          totalAmount: totalOrderAmount, // Set totalAmount based on the product's price and quantity
        });
        totalOrderAmount += orderTotalAmount;
      }
    }

    order.orderedProducts[0].totalAmount = totalOrderAmount;

    // Save the updated order
    await order.save();

    return res.status(200).send({ message: "Order Product Successfully." });
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};


module.exports.getUserOrders = async (req, res) => {
    try {
        const orders = await Order.find({ email: req.user.email });
        res.send(orders);
    } catch (err) {
        console.error(err);
        res.status(500).send({ error: 'An error occurred while fetching orders.' });
    }
};

module.exports.getAllOrders = (req,res)=>{
    return Order.find({}).then(result=>{
        return res.send(result)
    })
};

module.exports.productToShip = (req, res) => {

    let updateActiveField = {
        isPending: false
    }

    return Order.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then((user, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

module.exports.productDeliver = (req, res) => {

    let updateActiveField = {
        isActive: false
    }

    return Order.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then((user, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};


// GOODS for multiple but need to refactor the total amount
// const User = require("../models/User");
// const Order = require("../models/Order");

// module.exports.orderAProducts = async (req, res) => {
//   console.log(req.user.id);

//   if (req.user.isAdmin) {
//     return res.status(403).send("Action Forbidden"); // Use 403 for forbidden actions
//   }

//   try {
//     const user = await User.findById(req.user.id);

//     let order = await Order.findOne({
//       "Order.email": req.user.email
//     });

//     // Create a new order if it doesn't exist
//   if (!order) {
//     order = new Order({
//       firstName: user.firstName,
//       lastName: user.lastName,
//       email: user.email,
//       mobileNo: user.mobileNo,
//       orderedProducts: [
//         {
//           products: [],
//           totalAmount: 0, // Initialize totalAmount to 0
//         },
//       ],
//     });
//   }

//     // Calculate the totalAmount for the entire order based on the sum of product totalAmounts
//     const orderTotalAmount = order.orderedProducts.reduce((total, product) => total + product.totalAmount, 0);

//     // Update the totalAmount of the entire order
//     order.orderedProducts.forEach((product, index) => {
//       product.totalAmount = orderTotalAmount; // Set each product's totalAmount to the order's totalAmount
//     });

//     const requestBody = req.body;

//     if (Array.isArray(requestBody) && requestBody.length >= 1) {
//       // Loop through the products in the request body
//       for (let i = 0; i < requestBody.length; i++) {
//         const product = requestBody[i];

//         // Add the product to the user's order based on its index
//         order.orderedProducts[0].products.push({
//           productId: product.product_id,
//           name: product.name,
//           price: product.price,
//           quantity: product.quantity,
//           totalAmount: orderTotalAmount, // Set totalAmount based on the product's price and quantity
//         });
//       }
//     }

//     // Save the updated order
//     await order.save();

//     return res.status(200).send({ message: "Order Product Successfully." });
//   } catch (error) {
//     return res.status(500).send({ error: error.message });
//   }
// };




// GOOD for single poduct only
// const User = require("../models/User");
// const Order = require("../models/Order");

// module.exports.orderAProducts = async (req, res) => {
//   console.log(req.user.id);

//   if (req.user.isAdmin) {
//     return res.status(403).send("Action Forbidden"); // Use 403 for forbidden actions
//   }

//   try {
//     const user = await User.findById(req.user.id);

//     let order = await Order.findOne({
//       "Order.email": req.user.email
//     });

//     // Create a new order if it doesn't exist
// 	if (!order) {
// 	  order = new Order({
// 	    firstName: user.firstName,
// 	    lastName: user.lastName,
// 	    email: user.email,
// 	    mobileNo: user.mobileNo,
// 	    orderedProducts: [
// 	      {
// 	        products: [],
// 	        totalAmount: 0, // Initialize totalAmount to 0
// 	      },
// 	    ],
// 	  });
// 	}

//     // Calculate the total amount for the new products
//     const newProductTotal = req.body.price * req.body.quantity;

//     // Add the new products to the user's order
//     order.orderedProducts[0].products.push({
//       productId: req.body.product_id,
//       name: req.body.name,
//       price: req.body.price,
//       quantity: req.body.quantity,
//     });

//     // Update the totalAmount by adding the new product's total
//     order.orderedProducts[0].totalAmount += newProductTotal;

//     // Save the updated order
//     await order.save();

//     return res.status(200).send({ message: "Order Product Successfully." });
//   } catch (error) {
//     return res.status(500).send({ error: error.message });
//   }
// };