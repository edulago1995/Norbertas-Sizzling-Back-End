const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price,
        img : req.body.img,
    });

    return newProduct.save().then((product, error) => {

        if (error) {
            return res.send(false);

        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};

module.exports.getAllProducts = (req,res)=>{
    return Product.find({}).then(result=>{
        return res.send(result)
    })
};

module.exports.getAllActiveProducts = (req,res)=>{
    return Product.find({isActive:true}).then(result=>{
        return res.send(result)
    })
};

module.exports.getProduct = (req,res)=>{
    return Product.findById(req.params.productId).then(result=>{
        return res.send(result)
    })
};

module.exports.searchProductByName = async (req, res) => {
    try {
      const { productName } = req.body;
  
      const products = await Product.find({
        name: { $regex: productName, $options: 'i' }
      });
  
      res.json(products);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

module.exports.updateProduct = (req,res)=>{

    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error)=>{
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
};

module.exports.archiveProduct = (req, res) => {

    let updateActiveField = {
        isActive: false
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
    .then((product, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

module.exports.activateProduct = (req, res) => {

    let updateActiveField = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
    .then((product, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

module.exports.getEmailsOfUsers = async (req, res) => {
  const productId = req.params.productId;

  try {
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    const userIds = product.userOrders.map(userOrder => userOrder.userId);

    const consumerUsers = await User.find({ _id: { $in: userIds } });

    const emails = consumerUsers.map(user => user.email);

    res.status(200).json({ userEmails: emails });

  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};