const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return res.send("Incorrect E-mail");
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}

			else{
				return res.send("Incorrect Password");
			}
		}
	})
	.catch(err=>res.send(err))
}

module.exports.orderAProducts = async (req, res) => {

	console.log(req.user.id);
	console.log(req.body.productId);

	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id).then(async (user) => {
	    const productId = req.body.productId;
	    const quantity = req.body.quantity;

	    const product = await Product.findById(productId);
	    const productPrice = product.price;

	    if (user.orderedProducts.length > 0) {
	        const existingOrder = user.orderedProducts[0];
	        const existingProductIndex = existingOrder.products.findIndex(
	            (existingProduct) => existingProduct.productId === productId
	        );

	        if (existingProductIndex !== -1) {
	            existingOrder.products[existingProductIndex].quantity += quantity;
	        } else {
	            existingOrder.products.push({
	                productId: productId,
	                name: product.name,
	                price: product.price,
	                quantity: quantity,
	            });
	        }

	        // Calculate the totalAmount for the current order
	        existingOrder.totalAmount = existingOrder.products.reduce(
	            (total, product) => total + product.quantity * productPrice,
	            0
	        );
	    } else {
	        // If there are no existing orders, create a new one
	        const newOrder = {
	            products: [
	                {
	                    productId: productId,
	                    name: product.name,
	                    price: product.price,
	                    quantity: quantity,
	                },
	            ],
	            totalAmount: quantity * productPrice, // Initial totalAmount for the new order
	        };

	        user.orderedProducts.push(newOrder);
	    }

	    return user.save().then((user) => true).catch((err) => err.message);
	});

	if(isUserUpdated !== true){
		return res.send({ message: isUserUpdated});
	}


	let isProductUpdated = await Product.findById(req.body.productId).then(product => {

		let buyer = {
			userId: req.user.id
		}

		product.userOrders.push(buyer);

		return product.save().then(product => true).catch(err => err.message);
	})

	if(isProductUpdated !== true){
		return res.send({ message: isProductUpdated});
	}

	if(isUserUpdated && isProductUpdated){
		return res.send({message: "Order Product Successfully."})
	}
};

module.exports.getDetails = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))

};

module.exports.adminAUser = (req, res) => {

    let updateActiveField = {
        isAdmin: true
    }

    return User.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then((user, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};


// ADDED

module.exports.unadminAUser = (req, res) => {

    let updateActiveField = {
        isAdmin: false
    }

    return User.findByIdAndUpdate(req.params.userId, updateActiveField)
    .then((user, error) => {

        if(error){
            return res.send(false)

        } else {
            return res.send(true)
        }
    })
    .catch(err => res.send(err))

};

module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then(result => {
            return res.send(result);
        })
        .catch(err => {
            console.error("Error fetching users:", err);
            return res.status(500).send("Internal Server Error");
        });
};

	module.exports.resetPassword = async (req, res) => {
		try {

		//console.log(req.user)
		//console.log(req.body)

		  const { newPassword } = req.body;
		  const { id } = req.user; // Extracting user ID from the authorization header
	  
		  // Hashing the new password
		  const hashedPassword = await bcrypt.hash(newPassword, 10);
	  
		  // Updating the user's password in the database
		  await User.findByIdAndUpdate(id, { password: hashedPassword });
	  
		  // Sending a success response
		  res.status(200).send({ message: 'Password reset successfully' });
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Internal server error' });
		}
	};


	module.exports.updateProfile = async (req, res) => {
		try {

			console.log(req.user);
			console.log(req.body);
			
		// Get the user ID from the authenticated token
		  const userId = req.user.id;
	  
		  // Retrieve the updated profile information from the request body
		  const { firstName, lastName, mobileNo } = req.body;
	  
		  // Update the user's profile in the database
		  const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ firstName, lastName, mobileNo },
			{ new: true }
		  );
	  
		  res.send(updatedUser);
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Failed to update profile' });
		}
	  }


// module.exports.deleteUserProduct = async (req, res) => {
//   const userId = req.user.id; // Assuming you have the authenticated user's ID in the req.user object
//   const productIdToDelete = req.params.productId;

//   console.log(req.user.id)

//   try {
//     // Find the user by ID and ensure they exist
//     const user = await User.findById(userId);

//     if (!user) {
//   return res.status(404).json({ message: 'User not found' });
// }

// const productIndex = user.orderedProducts[0].products.findIndex(
//   (product) => product.productId === productIdToDelete
// );

// if (productIndex === -1) {
//   return res.status(404).json({ message: 'Product not found in user\'s ordered products' });
// }

// user.orderedProducts[0].products.splice(productIndex, 1);

// await user.save();

// res.status(200).json({ message: 'Product deleted successfully', user });

//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ message: 'Server error' });
//   }
// };

module.exports.deleteUserProduct = async (req, res) => {
  const userId = req.user.id; // Assuming you have the authenticated user's ID in the req.user object
  const productIdToDelete = req.params.id;

  console.log(req.user.id)

  try {
    // Find the user by ID and ensure they exist
    const user = await User.findById(userId);

    if (!user) {
  return res.status(404).json({ message: 'User not found' });
}

const productIndex = user.orderedProducts[0].products.findIndex(
  (product) => product.id === productIdToDelete
);

if (productIndex === -1) {
  return res.status(404).json({ message: 'Product not found in user\'s ordered products' });
}

user.orderedProducts[0].products.splice(productIndex, 1);

await user.save();

res.status(200).json({ message: 'Product deleted successfully', user });

  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

//09-17 NEW!!! for user cart 

module.exports.incrementQuantity = (req, res) => {
  const productId = req.params.id;

  // Ensure that the user is authenticated and their ID is available in the request
  const userId = req.user.id; // Assuming the user ID is stored in req.user

  // Find the user's ordered products and update the quantity of the specified product
  User.findById(userId)
    .then(user => {
      if (!user) {
        // User not found
        return res.status(404).send("User not found");
      }

      // Assuming 'orderedProducts' is an array of objects with a 'products' field
      const orderedProduct = user.orderedProducts[0].products.find(product => product.id === productId);
      if (!orderedProduct) {
        // Product not found in user's ordered products
        return res.status(404).send("Product not found");
      }

      // Increment the quantity by 1 (you can adjust this as needed)
      orderedProduct.quantity += 1;

      // Save the updated user document
      return user.save();
    })
    .then(() => {
      // Successfully updated the quantity
      return res.send(true);
    })
    .catch(err => {
      console.error(err);
      return res.status(500).send("Internal server error"); // Internal server error
    });
};


module.exports.decrementQuantity = (req, res) => {
  const productId = req.params.id;

  // Ensure that the user is authenticated and their ID is available in the request
  const userId = req.user.id; // Assuming the user ID is stored in req.user

  // Find the user's ordered products and update the quantity of the specified product
  User.findById(userId)
    .then(user => {
      if (!user) {
        // User not found
        return res.status(404).send("User not found");
      }

      // Assuming 'orderedProducts' is an array of objects with a 'products' field
      const orderedProduct = user.orderedProducts[0].products.find(product => product.id === productId);
      if (!orderedProduct) {
        // Product not found in user's ordered products
        return res.status(404).send("Product not found");
      }

      if (orderedProduct.quantity > 0) {
        // Decrement the quantity by 1
        orderedProduct.quantity -= 1;

        // Save the updated user document
        return user.save()
          .then(() => {
            // Successfully updated the quantity
            return res.send(true);
          });
      } else {
        // Quantity is already at 0, return an appropriate response
        return res.status(400).send("Quantity cannot be less than 0");
      }
    })
    .then(() => {
      // Successfully updated the quantity
      return res.send(true);
    })
    .catch(err => {
      console.error(err);
      return res.status(500).send("Internal server error"); // Internal server error
    });
};

// EXPEREMENTAL OF LOGIN

module.exports.getAllUserEmails = (req, res) => {
    User.find({}, 'email') // Select only the 'email' field
        .then(result => {
            const emails = result.map(user => user.email);
            return res.send(emails);
        })
        .catch(err => {
            console.error("Error fetching user emails:", err);
            return res.status(500).send("Internal Server Error");
        });
};