const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

	mongoose.connect("mongodb+srv://b297edu:HSgtfWvjiRNXyRlA@batch-297.bc8fzg3.mongodb.net/Norbertas-Sizzling?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

	app.use("/users",userRoutes);
	app.use("/products", productRoutes);
	app.use("/orders", orderRoutes);

if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

module.exports = {app,mongoose};
