const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    orderedProducts : [
       {
        products : [
            {
                productId : {
                    type : String,
                    required : [true, "Product ID is required"]
                },
                name: {
                    type : String,
                    required : [true, "Product Name is required"]
                },
                price: {
                    type : Number,
                    required : [true, "Product Price is required"]
                },
                quantity : {
                    type : Number
                }

            }
        ],
        totalAmount : {
            type : Number
        },
        purchasedOn : {
                type : Date,
                default : new Date()
        },
        }
    ]
})

module.exports = mongoose.model("User", userSchema);