const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/login",userController.loginUser);

router.post("/orderProduct",verify,userController.orderAProducts);

router.get("/details", verify, userController.getDetails);

router.put("/:userId/admin", verify, verifyAdmin, userController.adminAUser);

router.put("/:userId/unadmin", verify, verifyAdmin, userController.unadminAUser);

router.get("/all", verify, verifyAdmin, userController.getAllUsers);

router.delete('/users/:userId/products/:productId', verify, userController.deleteUserProduct);

router.put('/reset-password', verify, userController.resetPassword);

router.put('/updateprofile', verify, userController.updateProfile);

router.delete('/:id', verify, userController.deleteUserProduct);

router.post('/:id/increment', verify, userController.incrementQuantity);

router.post('/:id/decrement', verify, userController.decrementQuantity);

router.get("/all/emails", userController.getAllUserEmails);

module.exports = router;