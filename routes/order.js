const express = require("express");
const orderController = require("../controllers/order");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

const router = express.Router();

router.post("/orderProduct",verify,orderController.orderAProducts);
router.get("/myorders",verify,orderController.getUserOrders);
router.get("/allorders",verify,orderController.getAllOrders);
router.put("/:userId/toship", verify, verifyAdmin, orderController.productToShip);
router.put("/:userId/deliver", verify, orderController.productDeliver);

module.exports = router;